DROP DATABASE IF EXISTS engine_that_could_db;
CREATE DATABASE IF NOT EXISTS engine_that_could_db;

USE engine_that_could_db;

CREATE TABLE city
(
    id   BIGINT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE station
(
    id      BIGINT AUTO_INCREMENT,
    name    VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
    city_id BIGINT      NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name, city_id),
    UNIQUE KEY (address, city_id),
    FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE neighbour_stations
(
    id                BIGINT AUTO_INCREMENT,
    first_station_id  BIGINT NOT NULL,
    second_station_id BIGINT NOT NULL,
    distance          DOUBLE DEFAULT 0.0,
    PRIMARY KEY (id),
    UNIQUE KEY (first_station_id, second_station_id),
    FOREIGN KEY (first_station_id) REFERENCES station (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (second_station_id) REFERENCES station (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE route
(
    id   BIGINT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE station_arrival_departure_time
(
    id                 BIGINT AUTO_INCREMENT,
    station_id         BIGINT NOT NULL,
    arrival_time       TIME    NOT NULL,
    departure_time     TIME    NOT NULL,
    priority           BIGINT UNSIGNED DEFAULT 1,
    day_ordinal_number BIGINT          DEFAULT 1,
    route_id           BIGINT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (station_id, route_id),
    UNIQUE KEY (priority, route_id),
    FOREIGN KEY (station_id) REFERENCES station (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (route_id) REFERENCES route (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE passenger_car_type
(
    id                   BIGINT AUTO_INCREMENT,
    name                 VARCHAR(255) NOT NULL,
    transportation_price DECIMAL(10, 2) DEFAULT 0.0,
    car_capacity         BIGINT        DEFAULT 0,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE train
(
    id   BIGINT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE passenger_car
(
    id                    BIGINT AUTO_INCREMENT,
    name                  VARCHAR(255) NOT NULL,
    passenger_car_type_id BIGINT      NOT NULL,
    train_id              BIGINT      NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name, train_id),
    FOREIGN KEY (passenger_car_type_id) REFERENCES passenger_car_type (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (train_id) REFERENCES train (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE planned_departure
(
    id       BIGINT AUTO_INCREMENT,
    route_id BIGINT NOT NULL,
    date     DATE    NOT NULL,
    train_id BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (train_id) REFERENCES train (id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (route_id) REFERENCES route (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE actual_departure
(
    planned_departure_id BIGINT,
    route_id             BIGINT NOT NULL,
    date                 DATE    NOT NULL,
    train_id             BIGINT,
    PRIMARY KEY (planned_departure_id),
    FOREIGN KEY (planned_departure_id) REFERENCES planned_departure (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (train_id) REFERENCES train (id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (route_id) REFERENCES route (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE additional_option
(
    id    BIGINT AUTO_INCREMENT,
    name  VARCHAR(255) NOT NULL,
    price DOUBLE DEFAULT 0.0,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE ticket_discount_type
(
    id       BIGINT AUTO_INCREMENT,
    name     VARCHAR(255) NOT NULL,
    discount DOUBLE DEFAULT 0.0,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE ticket_discount
(
    id                      BIGINT AUTO_INCREMENT,
    ticket_discount_type_id BIGINT NOT NULL,
    document_id             VARCHAR(255),
    PRIMARY KEY (id),
    UNIQUE KEY (ticket_discount_type_id, document_id),
    FOREIGN KEY (ticket_discount_type_id) REFERENCES ticket_discount_type (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE seat
(
    id               BIGINT AUTO_INCREMENT,
    name             VARCHAR(255) NOT NULL,
    passenger_car_id BIGINT      NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name, passenger_car_id),
    FOREIGN KEY (passenger_car_id) REFERENCES passenger_car (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE user
(
    id              BIGINT AUTO_INCREMENT,
    name        VARCHAR(40)  NOT NULL,
    surname        VARCHAR(40)  NOT NULL,
    password        VARCHAR(255) NOT NULL
);

CREATE TABLE ticket
(
    id                   BIGINT AUTO_INCREMENT,
    user_id              BIGINT,
    from_station         BIGINT NOT NULL,
    to_station           BIGINT NOT NULL,
    planned_departure_id BIGINT NOT NULL,
    seat_id              BIGINT NOT NULL,
    ticket_discount_id   BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (from_station) REFERENCES station (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (to_station) REFERENCES station (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (planned_departure_id) REFERENCES planned_departure (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (seat_id) REFERENCES seat (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (ticket_discount_id) REFERENCES ticket_discount (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE additional_option_ticket
(
    id                   BIGINT AUTO_INCREMENT,
    additional_option_id BIGINT NOT NULL,
    ticket_id            BIGINT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE booked_seat
(
    id                    BIGINT AUTO_INCREMENT,
    neighbour_stations_id BIGINT NOT NULL,
    planned_departure_id  BIGINT NOT NULL,
    seat_id               BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (neighbour_stations_id) REFERENCES neighbour_stations (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (planned_departure_id) REFERENCES planned_departure (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (seat_id) REFERENCES seat (id) ON DELETE CASCADE ON UPDATE CASCADE
);