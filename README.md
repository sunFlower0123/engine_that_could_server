### Versions ###

Java 8.0_231

Angular 8

MySQL 8.0.17

Spring: 2.1.3.RELEASE

Spring Data JPA 2.1.3.RELEASE

Elasticsearch: 7.3.2.RELEASE

Eureka server 2.1.3.RELEASE

Eureka client 2.1.3.RELEASE

Zuul server 2.1.3.RELEASE
